﻿# Host: 172.16.30.121  (Version: 5.1.69-log)
# Date: 2015-01-23 17:05:20
# Generator: MySQL-Front 5.3  (Build 4.75)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "launcherad"
#

DROP TABLE IF EXISTS `launcherad`;
CREATE TABLE `launcherad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) DEFAULT NULL,
  `apklink` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL COMMENT '持续时间(毫秒)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "launcherad"
#

INSERT INTO `launcherad` VALUES (1,'http://img.xiaocong.tv/uploadfile/application/picture/94b79c9b80b374e57a7e34cf2daf8e7c.jpg,http://img.xiaocong.tv/uploadfile/application/picture/d3ecb6e4259b11e94ba8279366a52337.jpg','http://oss.aliyuncs.com/xiaocong/recomme/apps/ad/MediaRender.apk',5000);

#
# Structure for table "recomme"
#

DROP TABLE IF EXISTS `recomme`;
CREATE TABLE `recomme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block` varchar(20) DEFAULT NULL COMMENT '区块',
  `subBlock` varchar(20) DEFAULT NULL COMMENT '子区块',
  `type` smallint(3) DEFAULT '1' COMMENT '类型[1文字2图片3视频4应用',
  `index` smallint(6) DEFAULT '0' COMMENT '标识',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `img` varchar(1000) DEFAULT NULL COMMENT '图片',
  `link` varchar(100) DEFAULT NULL COMMENT '连接',
  `content` varchar(100) DEFAULT NULL COMMENT '内容',
  `parent` int(11) DEFAULT '0' COMMENT '父',
  `layout` varchar(20) DEFAULT NULL COMMENT '布局',
  `packageName` varchar(30) DEFAULT NULL COMMENT '包名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

#
# Data for table "recomme"
#

/*!40000 ALTER TABLE `recomme` DISABLE KEYS */;
INSERT INTO `recomme` VALUES (1,'lancher','category',4,1,'热门游戏',NULL,NULL,NULL,0,'recommend',NULL),(2,'lancher','category',4,2,'影视分类',NULL,NULL,NULL,0,'filmFication',NULL),(3,'lancher','category',4,3,'应用精选',NULL,NULL,NULL,0,'appBoutique',NULL),(4,'lancher','category',4,4,'用户',NULL,NULL,NULL,0,'account',NULL),(6,'lancher','content',1,1,'杜卡迪赛车','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen1.png','','{\"packagename\":\"it.dtales.DucatiChallengeFree \",\"id\":1595528}',1,NULL,NULL),(7,'lancher','content',1,3,'僵尸尖叫','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen3.png','','{\"packagename\":\"net.mobigame.zombietsunami\",\"id\":269468}',1,NULL,NULL),(8,'lancher','content',1,4,'暗影格斗','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen4.png','','{\"packagename\":\"com.nekki.shadowfight \",\"id\":1596530}',1,NULL,NULL),(9,'lancher','content',1,5,'大麻烦','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen5.png','','{\"packagename\":\"com.waterise.wr012\",\"id\":1596117}',1,NULL,NULL),(10,'lancher','content',1,6,'小小银河','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen6.png','','{\"packagename\":\"com.bitmapgalaxy.lpg\",\"id\":224784}',1,NULL,NULL),(11,'lancher','content',1,7,'愤怒的小鸟','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen7.png','','{\"packagename\":\"com.rovio.angrybirds\",\"id\":221970}',1,NULL,NULL),(12,'lancher','content',1,8,'VR网球赛','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen8.png','','{\"packagename\":\"jp.co.sega.vtc \",\"id\":1596712}',1,NULL,NULL),(13,'lancher','content',1,9,'植物大战僵尸','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen9.png','','{\"packagename\":\"com.popcap.pvz\",\"id\":221746}',1,NULL,NULL),(14,'lancher','content',1,10,'小怪兽求包养','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen10.png','','{\"packagename\":\"com.venbrux.tntbf2 \",\"id\":1596158}',1,NULL,NULL),(15,'lancher','content',1,11,'滑雪大冒险','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen11.png','','{\"packagename\":\"com.yodo1tier1.cmmm.SkiSafari\",\"id\":305972}',1,NULL,NULL),(16,'lancher','content',1,12,'桥梁构造游乐场','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen12.png','','{\"packagename\":\"com.headupgames.bridgeconstructorplayground\",\"id\":222068}',1,NULL,NULL),(17,'lancher','content',1,13,'黏黏世界','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen13.png','','{\"packagename\":\"com.twodboy.worldofgoofull\",\"id\":1595668}',1,NULL,NULL),(18,'lancher','content',3,1,'搜狐视频','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/1.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/sohuvideotv.apk','{\"packagename\":\"com.sohutv.tv\",\"id\":0}',2,NULL,NULL),(19,'lancher','content',3,2,'PPTV','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/2.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/pplive.TV.apk','{\"packagename\":\"com.pplive.androidtv\",\"id\":0}',2,NULL,NULL),(20,'lancher','content',3,3,'暴风影音','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/3.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/baofeng.tv.apk','{\"packagename\":\"com.baofeng.tv\",\"id\":1759963}',2,NULL,NULL),(21,'lancher','content',3,4,'迅雷看看','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/4.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/XunleiKankan.apk','{\"packagename\":\"com.xunlei.kankan.tv\",\"id\":0}',2,NULL,NULL),(22,'lancher','content',3,5,'乐视视频','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/5.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/letv.apk','{\"packagename\":\"com.letv.tv\",\"id\":0}',2,NULL,NULL),(23,'lancher','content',3,6,'腾讯视频','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/6.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/qqtv.apk','{\"packagename\":\"com.tencent.qqlivehd\",\"id\":0}',2,NULL,NULL),(24,'lancher','content',3,7,'moreTV','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/7.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/MoreTV_Gen_Latest.apk','{\"packagename\":\"com.moretv.android\",\"id\":0}',2,NULL,NULL),(25,'lancher','content',3,8,'爱奇艺','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/8.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/aiqiyi.apk','{\"packagename\":\"com.qiyi.video\",\"id\":0}',2,NULL,NULL),(26,'lancher','content',3,9,'快播','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/9.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/qvod.apk','{\"packagename\":\"com.qvod.player\",\"id\":0}',2,NULL,NULL),(27,'lancher','content',3,10,'优酷视频','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/10.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/youku.apk','{\"packagename\":\"com.youku.tv\",\"id\":0}',2,NULL,NULL),(28,'lancher','content',3,11,'百度视频','http://oss.aliyuncs.com/xiaocong/recomme/images/movie/11.png','http://oss.aliyuncs.com/xiaocong/recomme/apps/BaiduCloud2TV.apk','{\"packagename\":\"com.baidu.tv.app\",\"id\":0}',2,NULL,NULL),(30,'lancher','content',3,1,'更多游戏','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx1.png','','',3,NULL,NULL),(31,'lancher','content',3,2,'喷气机','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx2.png','','{\"packagename\":\"com.halfbrick.jetpackjoyridefree \",\"id\":1595513}',3,NULL,NULL),(32,'lancher','content',3,3,'颠簸之路','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx3.png','','{\"packagename\":\"laubak.android.game.bad.roads\",\"id\":224749}',3,NULL,NULL),(33,'lancher','content',3,4,'不再犹豫','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx4.png','','{\"packagename\":\"com.digitalreality.sinemora\",\"id\":1596455}',3,NULL,NULL),(38,'lancher','content',3,5,'3D纽约过山车','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx5.png','','{\"packagename\":\"com.digitalchocolate.rollnyfullpa \",\"id\":1595471}',3,NULL,NULL),(39,'lancher','content',3,6,'小贼物语','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx6.png','','{\"packagename\":\"com.roviostars.tinythief \",\"id\":1596360}',3,NULL,NULL),(40,'lancher','content',3,7,'豆腐忍者','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx7.png','','{\"packagename\":\"com.devolverdigital.tofu2\",\"id\":1595517}',3,NULL,NULL),(41,'lancher','content',3,8,'彩虹独角兽','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx8.png','','{\"packagename\":\"com.pikpok.rua2\",\"id\":1596365}',3,NULL,NULL),(42,'lancher','content',3,9,'僵尸公路','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/yyjx9.png','','{\"packagename\":\"com.auxbrain.zombie_highway\",\"id\":1596332}',3,NULL,NULL),(43,'lancher','content',3,10,'七星传说','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/qxcs.png','','{\"packagename\":\"com.idealdimension.sevenstars2\",\"id\":1595479}',3,NULL,NULL),(44,'lancher','content',3,11,'急速冲击','http://oss.aliyuncs.com/xiaocong/recomme/images/boutiqueapp/jscj.png','','{\"packagename\":\"com.mediocre.smashhit\",\"id\":1637788}',3,NULL,'com.digitalreality.sinemora'),(49,'lancher','content',1,14,'沙滩车闪电战','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen14.png','','{\"packagename\":\"com.vectorunit.yellow\",\"id\":1595525}',1,NULL,NULL),(52,'lancher','content',4,1,'','http://oss.aliyuncs.com/xiaocong/recomme/images/user/1/img.png','','',4,NULL,NULL),(53,'lancher','content',4,2,'','http://oss.aliyuncs.com/xiaocong/recomme/images/user/2/img0.png,http://oss.aliyuncs.com/xiaocong/recomme/images/user/2/img1.png','','',4,NULL,NULL),(54,'lancher','content',4,3,'','http://oss.aliyuncs.com/xiaocong/recomme/images/user/3/img.png','','',4,NULL,NULL),(55,'lancher','content',4,4,'','http://oss.aliyuncs.com/xiaocong/recomme/images/user/4/img.png','','',4,NULL,NULL),(56,'lancher','content',4,5,'','http://oss.aliyuncs.com/xiaocong/recomme/images/user/5/img.png','','',4,'',''),(57,'lancher','content',4,6,'','http://oss.aliyuncs.com/xiaocong/recomme/images/user/6/img.png','','',4,'',''),(58,'lancher','content',1,2,'找你妹','http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen2_title.png,http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen2_renwu.png,http://oss.aliyuncs.com/xiaocong/recomme/images/hotgame/remen2_bg.png','','{\"packagename\":\"org.funship.findsomething.withRK\",\"id\":222573}',1,'',''),(59,'lancher','content',1,1,'','http://oss.aliyuncs.com/xiaocong/recomme/images/ad/xc_start_bg.jpg','http://oss.aliyuncs.com/xiaocong/recomme/apps/ad/MediaRender.apk',NULL,5,NULL,NULL);
/*!40000 ALTER TABLE `recomme` ENABLE KEYS */;

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `mobile` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "user"
#

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'test','098f6bcd4621d373cade4e832627b4f6',1,NULL),(2,'admin','21232f297a57a5a743894a0e4a801fc3',NULL,NULL),(3,'admin11','21232f297a57a5a743894a0e4a801fc3',1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

#
# Structure for table "voide"
#

DROP TABLE IF EXISTS `voide`;
CREATE TABLE `voide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "voide"
#

/*!40000 ALTER TABLE `voide` DISABLE KEYS */;
INSERT INTO `voide` VALUES (1,'222',2,'222222222',NULL,'222222');
/*!40000 ALTER TABLE `voide` ENABLE KEYS */;
