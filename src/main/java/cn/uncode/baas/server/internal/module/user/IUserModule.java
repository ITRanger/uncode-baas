package cn.uncode.baas.server.internal.module.user;

import java.util.List;
import java.util.Map;

import cn.uncode.baas.server.exception.ValidateException;

public interface IUserModule {
	
	String signup(Object param) throws ValidateException, Exception;
	
	String login(Object param) throws ValidateException, Exception;
	
	void logout(Object param);
	
	boolean checkToken(Object param);
	
	Map<String, Object> getToken(Object param);
	
	boolean checkUsername(Object param) throws ValidateException;
	
	//***************
	// user
	//***************
	List<Map<String, Object>> findUser(Object param);
	
	Map<String, Object> findOneUser(Object param);
	
	int updateUser(Object param) throws ValidateException, Exception;
	
	//***************
	// group
	//***************
	List<Map<String, Object>> findGroup(Object param);
	
	Map<String, Object> findOneGroup(Object param);
	
	int insertGroup(Object param) throws ValidateException, Exception;
	
	int updateGroup(Object param) throws ValidateException, Exception;
	
	int removeGroup(Object param) throws Exception;
	
	//***************
	// role
	//***************
	List<Map<String, Object>> findRole(Object param);
	
	Map<String, Object> findOneRole(Object param);
	
	int insertRole(Object param) throws ValidateException, Exception;
	
	int updateRole(Object param) throws ValidateException, Exception;
	
	int removeRole(Object param) throws Exception;
	
	//***************
	// acl
	//***************
	
	Map<String, Object> findOneAcl(Object param);
	
	int insertAcl(Object param) throws ValidateException, Exception;
	
	int updateAcl(Object param) throws ValidateException, Exception;
	
	int removeAcl(Object param) throws Exception;
	
	//***************
	// usercfg
	//***************
	
	Map<String, Object> findOneUserCFG(Object param);
	
	int insertUserCFG(Object param) throws ValidateException, Exception;
	
	int updateUserCFG(Object param) throws ValidateException, Exception;
	

}
