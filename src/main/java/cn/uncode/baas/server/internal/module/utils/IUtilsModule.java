package cn.uncode.baas.server.internal.module.utils;

import cn.uncode.baas.server.exception.ValidateException;
import cn.uncode.baas.server.internal.module.rest.IRestModule;

public interface IUtilsModule {
	
	IRestModule getRest();
	
	Object httpGet(String url);
	
	Object httpPost(String url, Object param);
	
	String md5(String value)throws Exception;
	
	String currentTime();
	
	long serverTime();
	
	void sendMqttMsg(Object param) throws ValidateException;
	
	long activityLastTime();
}
